import React from 'react';
import {NavLink} from 'react-router-dom';

import './Navbar.css';
const Navbar = () => {
  return (
    <div id="Navbar">
      <NavLink className="link" to="/" activeClassName="active" exact>
        Add Book
      </NavLink>
      <NavLink className="link" to="/list" activeClassName="active">
        Book List
      </NavLink>
    </div>
  );
};

export default Navbar;
