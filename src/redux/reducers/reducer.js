export default (state = [], action) => {
  switch (action.type) {
    case 'GET_BOOK':
      return {...state, book: action.payload};
    case 'ADD_BOOK':
      return {...state, book: action.payload};
    case 'UPDATE_BOOK':
      return {...state, book: action.payload};
    case 'DELETE_BOOK':
      return {...state, book: action.payload};
    default:
      return state;
  }
};
