import axios from 'axios';

export const getBook = () => {
  return dispatch => {
    axios
      .get('http://localhost:3006/')
      .then(function (response) {
        dispatch({
          type: 'GET_BOOK',
          payload: response.data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const addBook = adddata => {
  return dispatch => {
    axios
      .post('http://localhost:3006/add', {
        data: adddata,
      })
      .then(function (response) {
        console.log(response);
        dispatch({
          type: 'ADD_BOOK',
          payload: response.data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const updateBook = updatadata => {
  return dispatch => {
    axios
      .post('http://localhost:3006/update', {
        data: updatadata,
      })
      .then(function (response) {
        console.log(response.data);
        dispatch({
          type: 'UPDATE_BOOK',
          payload: response.data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};
export const deleteBook = id => {
  return dispatch => {
    axios
      .delete(`http://localhost:3006/delete/${id}`)
      .then(function (response) {
        dispatch({
          type: 'DELETE_BOOK',
          payload: response.data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};
