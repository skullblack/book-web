import React, {useEffect} from 'react';
import './List.css';
import {useHistory} from 'react-router-dom';
import {getBook, deleteBook} from '../redux/action/bookAction';
import {connect} from 'react-redux';
const List = props => {
  let history = useHistory();

  const handleEditBook = bookId => {
    history.push(`/${bookId}`);
  };

  const handleDeleteBook = bookId => {
    props.deleteBook(bookId);
  };
  return (
    <div id="List">
      <div className="container p-5">
        <div className="row ">
          {props.books
            ? props.books.map((obj, i) => (
                <div key={i} className="col-12 col-sm-12 col-md-6 mb-5">
                  <div className="card align-items-center">
                    <div className="card-image" style={{backgroundImage: `url('${obj.bookcover}')`}}></div>
                    <div className="card-body w-100">
                      <div className="row">
                        <div className="col-6">
                          <div>
                            <small>ชื่อหนังสือ : </small>
                          </div>
                          <div>{obj.bookname}</div>
                        </div>
                        <div className="col-6">
                          <div>
                            <small>ชื่อคนแต่ง : </small>
                          </div>
                          <div>{obj.author}</div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-6">
                          <div>
                            <small>วันที่อ่าน : </small>
                          </div>
                          <div>{obj.firstdate}</div>
                        </div>
                        <div className="col-6">
                          <div>
                            <small>วันที่อ่านจบ :</small>
                          </div>
                          <div>{obj.enddate}</div>
                        </div>
                      </div>
                    </div>
                    <div className="w-100 d-flex justify-content-end pb-2 pr-2">
                      <div onClick={() => handleEditBook(obj.id)} className="btn btn-warning ml-2">
                        แก้ไข
                      </div>
                      <div onClick={() => handleDeleteBook(obj.id)} className="btn btn-danger ml-2">
                        ลบ
                      </div>
                    </div>
                  </div>
                </div>
              ))
            : ''}
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    books: state.book.book,
  };
};

export default connect(mapStateToProps, {getBook, deleteBook})(List);
