import React, {useEffect, useState, useRef} from 'react';
import {get} from 'lodash';
import './Home.css';
import {useParams, useHistory} from 'react-router-dom';
import {getBook, addBook, updateBook} from '../redux/action/bookAction';
import {connect} from 'react-redux';
const Home = props => {
  let {id} = useParams();
  let history = useHistory();
  const bookcover = useRef();
  const bookname = useRef();
  const author = useRef();
  const firstdate = useRef();
  const enddate = useRef();
  useEffect(() => {
    if (id) {
      setData();
    }
  }, [props.books]);

  const setData = () => {
    var dataSet = get(props, 'books', []).filter(fil => fil.id == id);
    if (dataSet.length) {
      bookcover.current.value = dataSet[0].bookcover;
      bookname.current.value = dataSet[0].bookname;
      author.current.value = dataSet[0].author;
      firstdate.current.value = dataSet[0].firstdate;
      enddate.current.value = dataSet[0].enddate;
    }
  };

  const handleSubmit = e => {
    e.preventDefault();
    var data = {
      id,
      bookcover: e.target.bookcover.value,
      bookname: e.target.bookname.value,
      author: e.target.author.value,
      firstdate: e.target.firstdate.value,
      enddate: e.target.enddate.value,
    };
    if (id) {
      props.updateBook(data);
      console.log('tan');
    } else {
      props.addBook(data);
    }
    e.target.reset();
    history.push('/');
  };
  return (
    <div id="Home">
      <form id="form" onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="bookcover">รูปปก หนังสือ</label>
          <input
            ref={bookcover}
            type="text"
            className="form-control"
            id="bookcover"
            placeholder="https://inventage.co/image/logo/logo.png"
            required
          />
        </div>
        <div className="form-row">
          <div className="form-group col-md-6">
            <label htmlFor="bookname">ชื่อหนังสือ</label>
            <input
              ref={bookname}
              type="text"
              className="form-control"
              id="bookname"
              placeholder="๋JavaScript"
              required
            />
          </div>
          <div className="form-group col-md-6">
            <label htmlFor="author">ชื่อคนเขียน</label>
            <input ref={author} type="text" className="form-control" id="author" placeholder="Jonh" required />
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col-md-6">
            <label htmlFor="firstdate"> วันที่เริ่มอ่าน </label>
            <input ref={firstdate} type="date" className="form-control" id="firstdate" required />
          </div>
          <div className="form-group col-md-6">
            <label htmlFor="enddate">วันที่อ่านจบ</label>
            <input ref={enddate} type="date" className="form-control" id="enddate" required />
          </div>
        </div>
        <button type="submit" className="btn btn-primary">
          บันทึก
        </button>
      </form>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    books: state.book.book,
  };
};

export default connect(mapStateToProps, {getBook, addBook, updateBook})(Home);
