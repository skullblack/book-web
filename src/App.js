import React, {useEffect} from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
import {connect} from 'react-redux';
import Navbar from './components/Navbar';
import Home from './pages/Home';
import List from './pages/List';
import {getBook} from './redux/action/bookAction';

function App(props) {
  useEffect(() => {
    props.getBook();
  }, []);

  return (
    <div className="App">
      <Router>
        <div>
          <Navbar />

          <Switch>
            <Route path="/list">
              <List />
            </Route>
            <Route path="/:id?">
              <Home />
            </Route>
            {/* <Route path="/">
              <Home />
            </Route> */}
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default connect(null, {getBook})(App);
