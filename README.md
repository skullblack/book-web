# Getting Started

clone project

เปิด command line แล้วเข้าไปที่ project file แล้วใช้คำสั่ง

### `docker-compose up -d --build`

เช็คว่า project ถูก run หรือยังด้วยคำสั่ง

### `docker ps -a`

ถ้าไม่ถูก run ให้ใช้คำสั่ง

### `docker start my_container`

เปิด localhost:3000
